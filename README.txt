INTRODUCTION
------------
This module provides a way to add a background image to a specific selector.
You can enable the background container per content type
and set a site wide default for each content type.
You can also enable this background for each node so that users can add an image
as a background to a specific section of their node.

* For a full description of the module, visit the project page:
https://www.drupal.org/sandbox/kevinvb/2552443

 * To submit bug reports and feature suggestions, or to track changes:
 https://www.drupal.org/project/issues/2552443

 REQUIREMENTS
------------
This module requires the following modules:

There are no requirements for this module.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.



 CONFIGURATION
-------------

* Enable background for every content type by editing the content type.

* When you choose to enable the per node override you will be able to add
a background container for each node of that type.

TROUBLESHOOTING
---------------
* If the background settings doesn't show check the following:
  - Check if the background module is installed and enabled

  - Check if you enable the background settings for your content type.

  - Check if node-overide is enabled

  - Check if you are on the right type of content

* If the image isn't shown

  - Check if you configured the node or content type with default settings

  - Check if the CSS selector is present in the DOM structure

  - Check if your node is displayed in full mode

MAINTAINERS
-----------
Current maintainers:
  * Kevin Van Belle (KevinVb) - https://www.drupal.org/u/kevinvb
