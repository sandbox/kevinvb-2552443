/**
 * @file
 * Handles the fieldsummary on node pages.
 */

(function ($) {
  "use strict";
  Drupal.behaviors.background_containerFieldsetSummaries = {
    attach: function (context) {
      $('fieldset.background-container-form', context).drupalSetSummary(function (context) {
        var text;
        var used = false;
        if ($('.form-item-background-container-enable input', context).is(':checked')) {
          text = Drupal.t('Background enabled for nodes.');
          used = true;
        }
        if ($('.form-item-background-container-use input', context).is(':checked')) {
          if (used) {
            text += '<br>' + Drupal.t('Default background set for nodes.');
          }
          else {
            text = Drupal.t('Default background set for nodes.');
            used = true;
          }
        }
        if (!used) {
          text = Drupal.t('No background used.');
        }
        return text;
      });
    }
  };
})(jQuery);
